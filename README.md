# OpenML dataset: Used-Cars-In-Saudi-Arabia

https://www.openml.org/d/43474

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Yallamotor is website in ksa have a collection of used vehicles for sale.
I used the Yallamotor website to create dataset of used vehicles in KSA.
Content
Dataset includes ( 2287 ) vehicles information like price ,car model ,etc .. 
Acknowledgements
The data in this dataset has been scraped using BeautifulSoup from the Yallamotor website.
Inspiration
Data will be handy to study and practice different models and approaches.
As a further step you can use regression model to predict the car price based on the different features.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43474) of an [OpenML dataset](https://www.openml.org/d/43474). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43474/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43474/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43474/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

